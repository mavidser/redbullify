import * as jwt from 'jsonwebtoken';
import { JWT_SECRET } from '../lib/mockdata';
import { controller } from '../lib/promisify_controller';

const jwtExpiry = 3600;

/**
 * @api {post} /authenticate Authenticate user
 * @apiDescription Accepts username and password to return the access token
 * @apiName authenticate
 * @apiGroup Authentication
 * @apiPermission user
 *
 * @apiParam (Body) {String} username Username of the user.
 * @apiParam (Body) {String} password Password for the username.
 *
 * @apiSuccess {Object} success Signifies that the API call was a success
 * @apiSuccess {String} success.access_token The access token
 * @apiSuccess {Number} success.expires_in The time suration for which the access token is valid
 * @apiSuccess {String} success.token_type The token type to send with the authorization header
 *
 * @apiError (Error 4xx) 401 Invalid credentials.
 * @apiError (Error 5xx) 500 Server Error occurred.
 *
 * @apiExample {curl} Example usage:
 * curl -X POST \
 *      -d 'username=admin&password=admin' \
 *      http://localhost:3000/authenticate
 */
export let authenticate = controller(async (req) => {
  return {
    access_token: generateJWT(req.user),
    expires_in: jwtExpiry,
    token_type: 'JWT',
  };
});

function generateJWT(username: string) {
  return jwt.sign({username}, JWT_SECRET, {
    expiresIn: jwtExpiry,
  });
}
