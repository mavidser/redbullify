import { APIError } from '../../lib/api_error';
import { controller } from '../../lib/promisify_controller';
import { Item } from '../../models/Item';

/**
 * @api {put} /api/item/:id Update existing item
 * @apiDescription Updates an existing item which the user created.
 * @apiName updateItem
 * @apiGroup Items
 *
 * @apiParam (Param) {String} id ID of the item.
 * @apiParam (Body) {String} name name of the item.
 * @apiParam (Body) {String} price price of the item.
 * @apiParam (Body) {String} [description] description of the item.
 * @apiParam (Body) {String} [category] category of the item.
 *
 * @apiSuccess {boolean} success true if successfully updated
 *
 * @apiError (Error 4xx) 400 Invalid request parameters. Returns the potential reasons.
 * @apiError (Error 4xx) 403 Not authorized to modify this item
 * @apiError (Error 4xx) 401 Invalid access token. Not authorized to call the API.
 * @apiError (Error 5xx) 500 Server Error occurred.
 *
 * @apiExample {curl} Example usage:
 * curl -X PUT \
 *      -d 'name=Foo&price=2' \
 *      -H 'authorization: JWT <token>' \
 *      http://localhost:3000/api/item/<item_id>
 */
export const updateItem = controller(async (req) => {
  req.sanitizeBody('name').trim(' ');
  req.sanitizeBody('category').trim(' ');
  req.sanitizeBody('description').trim(' ');
  let item;
  try {
    item = await Item.findById(req.params.id);
    if (!item) {
      throw new APIError('Item not found');
    }
    if (item.author !== req.user) {
      throw new APIError('Not authorized to modify this item', 403);
    }
    if (req.body.category !== undefined) {
      item.category = req.body.category;
    }
    if (req.body.description !== undefined) {
      item.description = req.body.description;
    }
    if (req.body.name !== undefined) {
      item.name = req.body.name;
    }
    if (req.body.price !== undefined) {
      item.price = req.body.price;
    }
  } catch (err) {
    if (err instanceof APIError) {
      throw err;
    }
    throw new APIError('Item not found');
  }
  try {
    await Item.update({ _id: req.params.id }, { $set: item }, { runValidators: true });
    return true;
  } catch (err) {
    if (err.name === 'ValidationError') {
      const message = Object.keys(err.errors).map((error) => err.errors[error].message).join('. ');
      throw new APIError(message);
    }
    throw err;
  }
});
