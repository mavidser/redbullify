import { APIError } from '../../lib/api_error';
import { controller } from '../../lib/promisify_controller';
import { validate } from '../../lib/validate_request';
import { Item } from '../../models/Item';

/**
 * @api {get} /api/item/:id Get Item by Id
 * @apiDescription Returns the details of the required item.
 * @apiName getItemById
 * @apiGroup Items
 *
 * @apiParam (Param) {String} id ID of the item.
 *
 * @apiSuccess {Object} success The item
 * @apiSuccess {boolean} [success.amAuthor] True if current user is the author of the item.
 * @apiSuccess {string} success.category Item's category
 * @apiSuccess {string} success.description Item's description
 * @apiSuccess {string} success.id Item's ID
 * @apiSuccess {string} success.name Item's name
 * @apiSuccess {number} success.price Item's price
 *
 * @apiError (Error 4xx) 400 invalid id. invalid id. must be alphanumeric
 * @apiError (Error 4xx) 400 Item not found
 * @apiError (Error 4xx) 401 Invalid access token. Not authorized to call the API.
 * @apiError (Error 5xx) 500 Server Error occurred.
 *
 * @apiExample {curl} Example usage:
 * curl -X GET \
 *      -H 'authorization: JWT <token>' \
 *      http://localhost:3000/api/item/<item_id>
 */
export const getItemById = controller(async (req) => {
  req.checkParams('id', 'invalid id. must be alphanumeric')
    .isAlphanumeric();

  await validate(req);

  try {
    const item = await Item.findById(req.params.id);
    const responose = {
      amAuthor: item.author === req.user ? true : undefined,
      category: item.category,
      description: item.description,
      id: item._id,
      name: item.name,
      price: item.price,
    };

    return responose;
  } catch (err) {
    throw new APIError('Item not found');
  }
});
