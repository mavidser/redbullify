import * as bodyParser from 'body-parser';
import * as dotenv from 'dotenv';
import * as express from 'express';
import * as mongoose from 'mongoose';
import * as morgan from 'morgan';
import 'source-map-support/register';
import { APIError } from './lib/api_error';
import logger from './lib/logger';
import expressValidator = require('express-validator');

dotenv.config({ path: '.env.example' });

import * as authenticationController from './controllers/authentication';
import { default as passport } from './lib/passport';
import apiRoutes from './routes/api';

if (process.env.NODE_ENV === 'test') {
  process.env.MONGODB_URI = process.env.MONGODB_URI_TEST;
}
mongoose.connect(process.env.MONGODB_URI);
mongoose.connection.on('error', () => {
  console.error('MongoDB connection error. Please make sure MongoDB is running.');
  process.exit();
});

const app = express();

app.set('port', process.env.PORT || 3000);
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressValidator());
app.use(passport.initialize());

app.get('/', (req: express.Request, res: express.Response) => {
  res.send('true');
});

app.use('/api', passport.authenticate('jwt', { session: false }));
app.use('/api', apiRoutes);
app.post('/authenticate', passport.authenticate('local', { session: false }), authenticationController.authenticate);

app.use((err: Error, req: express.Request, res: express.Response, next: express.NextFunction) => {
  if (err instanceof APIError) {
    res.status((err as APIError).status || 400).json({
      error: err.message,
    });
  } else {
    logger.error(err.stack);
    if (process.env.NODE_ENV === 'development') {
      res.status(500).send(err.stack);
    } else {
      res.status(500).json({
        error: 'Server Error occurred',
      });
    }
  }
});

export default app;
