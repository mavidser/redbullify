import { APIError } from './api_error';

export async function validate(req: Express.Request) {
  const validationResult = await req.getValidationResult();
  if (!validationResult.isEmpty()) {
    const errorMessage = validationResult.array().map((item) => item.msg).join('. ');
    throw new APIError(errorMessage);
  }
}
