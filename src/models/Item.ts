import * as mongoose from 'mongoose';

(mongoose as any).Promise = global.Promise;

export type ItemModel = mongoose.Document & {
  author: string,
  category: string,
  description: string,
  name: string,
  price: number,
};

const ItemSchema = new mongoose.Schema({
  author: String,
  category: {
    maxlength: [50, 'Category cannot be more than 50 characters'],
    type: String,
  },
  description: {
    maxlength: [500, 'Description cannot be more than 500 characters'],
    type: String,
  },
  name: {
    maxlength: [50, 'Name cannot be more than 50 characters'],
    required: [true, 'Name is required for the item'],
    type: String,
  },
  price: {
    min: [0.01, 'Price cannot be less than 0.01'],
    required: [true, 'Price is required for the item'],
    type: Number,
  },
}, { timestamps: true });

export const Item = mongoose.model<ItemModel>('Item', ItemSchema);
