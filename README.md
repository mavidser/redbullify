# API

Needs [MongoDB](https://docs.mongodb.com/manual/installation/) and [NodeJS 6+](https://docs.mongodb.com/manual/installation/) to run.

Upon installing and starting MongoDB, put the details in the `.env.example` file.

## Getting started

```bash
git clone https://gitlab.com/mavidser/redbullify.git
cd redbullify
npm install
npm start
```

## Running tests

```bash
npm test
```

## API Usage

The server starts on port `3000` by default.

Your first step would be to obtain an access token for to use with the API. The server has demo credentials built with username and password being admin.

```
curl -X POST \
     -d 'username=admin&password=admin' \
     http://localhost:3000/authenticate
```

This returns a JSON reponse containing an access token. Typically, it is valid for only one hour, but for the purpose of this readme file, this access token should for about 7 years since the time of writing.

```
{
  "success": {
    "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiaWF0IjoxNDk2NTA5NTk3LCJleHAiOjE3MjU0Njk1OTd9.jPz_Yn0j6w2q-w8roQ5Wboht-Kic5ieyYadZcymYtEU",
    "expires_in": 228960000,
    "token_type": "JWT"
  }
}
```

Use this API Token in the Authorization header to call any further endpoints.

To create an item:

```
curl -X POST \
     -d 'name=Foo&price=10.00&description=Bar' \
     -H 'authorization: JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiaWF0IjoxNDk2NTA5NTk3LCJleHAiOjE3MjU0Njk1OTd9.jPz_Yn0j6w2q-w8roQ5Wboht-Kic5ieyYadZcymYtEU' \
     http://localhost:3000/api/item/
```

To view the list of items present:

```
curl -X GET \
      -H 'authorization: JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiaWF0IjoxNDk2NTA5NTk3LCJleHAiOjE3MjU0Njk1OTd9.jPz_Yn0j6w2q-w8roQ5Wboht-Kic5ieyYadZcymYtEU' \
      http://localhost:3000/api/item/
```

Detailed documentation can be found on the [documentation page](https://mavidser.gitlab.io/redbullify).

## Deployment

If Node.JS and MongoDB are installed and running:

```
npm install
npm start
```

Else, if you've got docker installed:

```
docker-compose up
```

If you like heroku, follow these steps:

```bash
heroku create <app_name>
heroku git:remote -a <app_name>
heroku addons:create mongolab:sandbox
heroku config:set NPM_CONFIG_PRODUCTION=false #As we don't have a build pipeline yet, we need devDependencies to be installed for the typescript tools to generate javascript
heroku config:set NODE_ENV=production
git push heroku master
```

## License

MIT
