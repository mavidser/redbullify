import * as _ from 'lodash';

declare global {
  namespace jest {
    interface Matchers {
      toBeSortedBy(key: string, isAscending?: boolean): void;
    }
  }
}

function toBeSortedBy(input: object[], key: string, isAscending?: boolean) {
  const pass = _.every(input, (value, index, array) => {
    if (index === 0) {
      return true;
    }
    if (typeof array[index - 1][key] === 'string' && typeof value[key] === 'string')  {
      if (isAscending === false) {
        return array[index - 1][key].toLowerCase() >= value[key].toLowerCase();
      } else {
        return array[index - 1][key].toLowerCase() <= value[key].toLowerCase();
      }
    } else if (typeof array[index - 1][key] === 'number' && typeof value[key] === 'number')  {
      if (isAscending === false) {
        return array[index - 1][key] >= value[key];
      } else {
        return array[index - 1][key] <= value[key];
      }
    } else {
      return false;
    }
  });
  const order = isAscending === false ? 'descending' : 'ascending';
  let message;
  if (pass) {
    message = `The array should not be sorted by ${key} in ${order}`;
  } else {
    message = `The array should be sorted by ${key} in ${order}`;
  }
  return {
    message: () => message,
    pass,
  };
}

expect.extend({
  toBeSortedBy,
});
